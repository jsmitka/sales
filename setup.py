#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup, find_packages
except ImportError:
    import ez_setup
    ez_setup.use_setuptools()
    from setuptools import setup, find_packages

import os

setup(
    name = "django-sales",
    version = "0.1.3",
    url = 'https://bitbucket.org/jsmitka/sales/overview',
    download_url = 'https://bitbucket.org/jsmitka/sales/downloads',
    license = 'BSD',
    description = "Simple apps for Django",
    author = 'Jindrich Smitka',
    author_email = 'jsmitka@smita.info',
    packages = find_packages(),
    namespace_packages = ['sales'],
    #requires = ["django", "django-taggit", "sorl-thumbnail", "recaptcha-client", "South", "Markdown"]
    include_package_data = True,
    zip_safe = False,
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
    ]
)
