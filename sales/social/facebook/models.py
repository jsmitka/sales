# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext


class FacebookUser(models.Model):
    uid = models.CharField(max_length=64, primary_key=True, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    profile_url = models.CharField(max_length=255)
    access_token = models.CharField(max_length=255)


class FacebookPostContent(models.Model):
    content_hash = models.CharField(max_length=32, primary_key=True, unique=True)
    fid = models.CharField(max_length=128)


class FacebookAccounts(models.Model):
    aid = models.CharField(max_length=64)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    access_token = models.CharField(max_length=255)
    facebook_user = models.ForeignKey('FacebookUser')

