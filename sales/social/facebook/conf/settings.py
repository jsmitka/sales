# -*- coding: utf-8 -*-
from django.conf import settings


FACEBOOK_APP_ID = getattr(settings,
                          "FACEBOOK_APP_ID",
                          "")

FACEBOOK_APP_SECRET = getattr(settings,
                              "FACEBOOK_APP_SECRET",
                              "")

FACEBOOK_REQUIRED_PERMISSIONS = getattr(settings,
                                        "FACEBOOK_REQUIRED_PERMISSIONS",
                                        "read_stream")
