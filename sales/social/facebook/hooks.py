# -*- coding: utf-8 -*-
"""
Modul obsahuje funkce pro obsluhu signalu objektu.
"""


def __post():
    """
    Metoda pro zapis na facebook.
    """
    pass


def post_page():
    """
    Metoda zapise na zed stranky uzivatele.
    """
    pass


def post_me():
    """
    metoda zapise na zed uzivatele.
    """
    pass


def post_image():
    """
    Metoda posle obrazek do zadaneho alba.
    """
    pass


def post_album():
    """
    Metoda vytvori album.
    """
    pass


def post_event():
    """
    Metoda vytovri udalost.
    """
    pass
