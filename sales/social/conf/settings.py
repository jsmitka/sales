# -*- coding: utf-8 -*-
from django.conf import settings

"""
SOCIAL_POST_HOOKS - slouzi k definovani vazby mezi objekty aplikace
                    a socialnimi sitemi. Tedy ktery objekt se bude kam posilat,
                    jako novinky, obrazek, komantar atd.

Priklad:
    SOCIAL_POST_HOOKS = {
                            "facebook.post_me": "sales.articles.Article",
                            "facebook.post_page": "sales.news.NewsMessage",
                            "twitter.post": "record.Record",
                        }
"""
SOCIAL_POST_HOOKS = getattr(settings,
                            "SOCIAL_POST_HOOKS",
                            None)
