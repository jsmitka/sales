# -*- coding: utf-8 -*-
import re
import bisect

from sales.menu.conf import settings
from sales.menu.models import Menu


class MenuError(Exception):
    pass

class AMenu(object):
    """
    Abstraktni trida, kazda polozka menu, musi dedit od AMenu.
    """

    menu_dict = {}

    def __init__(self, name, url="/", submenu=None, parent=None):
        self.name = name
        self.url = url
        #Pokud se ma menu tridit a je submenu ruzne od None
        if settings.MENU_SORT and submenu is not None:
            self.submenu = sorted(submenu)
        else:
            self.submenu = submenu
        self.parent = parent
        self.selected = False
        self._step = 0

        self.__class__.menu_dict[self.url] = self

    def __iter__(self):
        return self

    def __len__(self):
        try:
            return len(self.submenu)
        except TypeError:
            return 0

    def __lt__(self, other):
        return (self.weight < other.weight)

    def _get_weight(self):
        '''
        Ziska vahu konkretniho menu.
        '''
        if settings.MENU_SORT:
            try:
                menu = Menu.objects.get(url=self.url)
            except Menu.DoesNotExist:
                menu = Menu(url=self.url)
                menu.save()

            weight = menu.weight
        else:
            weight = 0

        return weight
    weight = property(_get_weight)

    def next(self):
        if self._step >= len(self.submenu):
            self._step = 0
            raise StopIteration
        self._step = self._step + 1
        return self.submenu[self._step-1]

    def reset(self):
        self._step = 0

    def find(self, url, exact=False):
        try:
            return self.__class__.menu_dict[url]
        except KeyError:
            if settings.MENU_PROBABLY_MATCH and not exact:
                match = re.compile(r"(/?(.+/)*)(.+/?)", re.UNICODE)
                url_match = url
                while url_match != "":
                    url_match = match.sub("\g<1>", url_match)
                    try:
                        return self.__class__.menu_dict[url_match]
                    except KeyError:
                        pass

                raise MenuError("Given URL '%s' have not analogous item." % (url, ))

            else:
                raise MenuError("Given URL '%s' not match any item." % (url, ))

    def get_parent(self):
        menu = self
        while menu is not None:
            yield menu
            menu = menu.parent

    def toggle(self, url, boolen=True):
        menu_item = self.find(url)
        branch = menu_item.get_parent()
        for item in branch:
            item.selected = boolen

    def select(self, url):
        self.toggle(url, True)

    def unselect(self, url):
        self.toggle(url, False)


class SimpleMenu(AMenu):
    def __init__(self, name, url, submenu=None, parent=None):
        super(self.__class__, self).__init__(name=name,
                                             url=url,
                                             parent=parent,
                                             submenu=submenu)


class RootMenu(AMenu):
    def __init__(self):
        super(self.__class__, self).__init__(name="Root")
        self.submenu = []

    def register(self, submenu):
        if settings.MENU_SORT:
            bisect.insort_right(self.submenu, submenu)
        else:
            self.submenu.append(submenu)

    def unregister(self, submenu):
        self.submenu.remove(submenu)



menu = RootMenu()
