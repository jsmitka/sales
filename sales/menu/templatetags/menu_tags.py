# -*- coding: utf-8 -*-

from django import template
from django.template.loader import get_template
from django.template import Context
from django.template import Library
from django.template import RequestContext
from django.template import resolve_variable

from sales.menu.models import Menu
from sales.menu.menu import menu



register = Library()

#FIXME: Fce get_menu a get_submenu vraci podobna data,
#FIXME: tak by bylo vhodne udelat z nich jen jednu fci.
#FIXME: get_menu by mel mit pocet polozek, ktere se zobrazi.
@register.inclusion_tag("menu/snippets/menu.html", takes_context=True)
def get_menu(context, menu_group="master"):
    """
    Vrati menu skupiny "menu_group".
    
    context -- context
    menu_group -- urcita skupina menu
    """
    #menu = Menu.objects.get_menu_by_group(menu_group)
    #context.update({"menu_list": menu})
    #menu.reset()
    context.update({"menu_list": menu})
    return context


@register.inclusion_tag("menu/snippets/submenu.html", takes_context=True)
def get_submenu(context, level=1):
    submenu = None

    #FIXME: Nezrovna ciste reseni - nezobrazovani menu na hlavni strance
    request = context["request"]
    if request.path != "/":
        m = menu.submenu
        i = 0
        while m is not None and len(m) != 0:
            if i != level:
                for s in m:
                    if s.selected:
                        m = s
                        break
            else:
                submenu = m
                break

            i = i + 1




    context.update({"submenu": submenu})
    return context


@register.inclusion_tag("menu/snippets/breadcrumbs.html", takes_context=True)
def get_breadcrumbs(context):
    breadcrumbs = None
    context.update({"breadcrumbs": breadcrumbs})
    return context


@register.inclusion_tag("menu/snippets/sitemap.html", takes_context=True)
def get_sitemap(context):
    context.update({"sitemap_list": menu})
    return context
