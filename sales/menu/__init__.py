# -*- coding: utf-8 -*-


def autodiscover():
    """
    Auto-discover INSTALLED_APPS menu.py modules and fail silently when
    not present. This forces an import on them to register any admin bits they
    may want.
    """

    #import copy
    from django.conf import settings
    from django.utils.importlib import import_module
    from django.utils.module_loading import module_has_submodule

    # Odstraneni vlastni aplikace z hledani modulu menu.
    #apps = copy.copy(settings.INSTALLED_APPS)
    #apps.remove(__package__)
    #apps.remove(__name__)
    for app in settings.INSTALLED_APPS:
        if app != __name__:
            mod = import_module(app)
            # Attempt to import the app's admin module.
            try:
                #before_import_registry = copy.copy(site._registry)
                import_module('%s.menu' % app)
            except:
                # Reset the model registry to
                # the state before the last import as
                # this import will have to reoccur on the next request and this
                # could raise NotRegistered and AlreadyRegistered exceptions
                # (see #8245).
                #site._registry = before_import_registry

                # Decide whether to bubble up this error. If the app just
                # doesn't have an admin module, we can ignore the error
                # attempting to import it, otherwise we want it to bubble up.
                if module_has_submodule(mod, 'menu'):
                    raise
