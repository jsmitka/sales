# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext


# Create your models here.
class Menu(models.Model):
    url = models.CharField(verbose_name=ugettext(u"Odkaz"), 
                           max_length=255,
                           editable=False)
    weight = models.PositiveIntegerField(verbose_name=ugettext(u"Pořadí"), default=1)

    def __unicode__(self):
        return self.url

    class Meta:
        verbose_name = ugettext(u"Menu")
        verbose_name_plural = ugettext(u"Menu")
