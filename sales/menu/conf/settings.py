# -*- coding: utf-8 -*-
from django.conf import settings


# Pouziva se pro pripojeni (MENU_STATIC_APPEND), 
# resp. predrazeni (MENU_STATIC_PREPEND) polozek menu, ktere se nedaji ziskat
# z aplikaci.
#
# Pr.:
# Odkaz na mapu na seznamu. Pridanim do settings.py projektu tohoto.
#
#MENU_STATIC_APPEND = ({"url": "http://www.mapy.cz/",
#                       "name": "Mapy",
#                       "submenu": None},)

#TODO: Staticke menu se pridava zatim jen do hlavniho menu. 
#TODO: Dodelat pridani i do podmenu.

MENU_STATIC_APPEND = getattr(settings,
                             "MENU_STATIC_APPEND",
                             None)

MENU_STATIC_PREPEND = getattr(settings,
                              "MENU_STATIC_PREPEND",
                              None)

MENU_PROBABLY_MATCH = getattr(settings,
                              "MENU_PROBABLY_MATCH",
                              True)

MENU_SORT = getattr(settings,
                    "MENU_SORT",
                    False)
