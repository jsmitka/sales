# -*- coding: utf-8 -*-

from sales.menu import autodiscover
from menu import menu


class SetMenuSelected(object):
    def process_request(self, request):
        autodiscover()
        try:
            menu.select(request.path)
        except KeyError:
            pass

    def process_response(self, request, response):
        try:
            menu.unselect(request.path)
        except KeyError:
            pass
        return response
