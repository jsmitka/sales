# -*- coding: utf-8 -*-

from django.contrib import admin

from models import Menu

class MenuAdmin(admin.ModelAdmin):
    list_display = ("url", "weight",)

admin.site.register(Menu, MenuAdmin)
