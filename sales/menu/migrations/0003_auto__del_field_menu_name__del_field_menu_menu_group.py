# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting field 'Menu.name'
        db.delete_column('menu_menu', 'name')

        # Deleting field 'Menu.menu_group'
        db.delete_column('menu_menu', 'menu_group')


    def backwards(self, orm):
        
        # Adding field 'Menu.name'
        db.add_column('menu_menu', 'name', self.gf('django.db.models.fields.CharField')(default='Nahradit...', max_length=255), keep_default=False)

        # Adding field 'Menu.menu_group'
        db.add_column('menu_menu', 'menu_group', self.gf('django.db.models.fields.CharField')(default='master', max_length=255), keep_default=False)


    models = {
        'menu.menu': {
            'Meta': {'object_name': 'Menu'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'weight': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        }
    }

    complete_apps = ['menu']
