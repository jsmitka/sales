# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Adding model 'Menu'
        db.create_table('menu_menu', (
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('menu_group', self.gf('django.db.models.fields.CharField')(default='master', max_length=255)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('weight', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('menu', ['Menu'])
    
    
    def backwards(self, orm):
        
        # Deleting model 'Menu'
        db.delete_table('menu_menu')
    
    
    models = {
        'menu.menu': {
            'Meta': {'object_name': 'Menu'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_group': ('django.db.models.fields.CharField', [], {'default': "'master'", 'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'weight': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        }
    }
    
    complete_apps = ['menu']
