# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import DetailView, ListView

from sales.gallery.conf import settings
from sales.multimedia.models import ImageFile


class GalleryListView(ListView):
    paginate_by = settings.GALLERY_ITEMS_PER_PAGE
    allow_empty = True
    template_name = "gallery/list.html"

    def get_queryset(self):
        """
        Vytvori queryset pro fotky.

        Pr.:
            gallery - cela galerie
            gallery/mesto - fotky s tagy 'gallery' a 'mesto'
            gallery/mesto/noc - fotky s tagy 'gallery', 'mesto' a 'noc'

        a k tomu odpovidajici url:
            /gallery/
            /gallery/mesto/
            /gallery/mesto/noc/

        lepsi /gallery?mesto+noc ?
        """
        # Naplneni querysetu
        qs = ImageFile.objects.filter(tags__name=settings.GALLERY_TAG)
        for tag in self.used_tags:
            qs = qs.filter(tags=tag)

        return qs

    def get_context_data(self, **kwargs):
        context = super(GalleryListView, self).get_context_data(**kwargs)

        context["tags"] = self.tags
        context["used_tags"] = self.used_tags

        return context

    def _get_used_tags(self):
        if not hasattr(self,"__used_tags"):
            if self.request.method == "GET":
                try:
                    self.__used_tags = ImageFile.tags.filter(name__in=self.request.GET["tags"].split("+"))
                except KeyError:
                    self.__used_tags = []

        return self.__used_tags

    used_tags = property(_get_used_tags)

    def _get_tags(self):
        if not hasattr(self, "__tags"):
            self.__tags = ImageFile.tags.exclude(name=settings.GALLERY_TAG)
            for tag in self.used_tags:
                self.__tags = self.__tags.exclude(name=tag)
        return self.__tags

    tags = property(_get_tags)



class ImageDetailView(DetailView):
    queryset = ImageFile.objects.all()
    template_name = "gallery/detail.html"
