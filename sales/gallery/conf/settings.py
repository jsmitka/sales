# -*- coding: utf-8 -*-
from django.conf import settings
from sales.multimedia.conf import settings as multimedia_settings

GALLERY_TAG = getattr(settings,
                      "GALLERY_TAG",
                      "gallery")

GALLERY_ITEMS_PER_PAGE = getattr(settings,
                                 "GALLERY_ITEMS_PER_PAGE",
                                 multimedia_settings.MULTIMEDIA_ITEMS_PER_PAGE)
