# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *

from views import GalleryListView, ImageDetailView

urlpatterns = patterns('',
        url(r"^$", GalleryListView.as_view(), name="gallery.list"),
        url(r"^image/(?P<pk>\d+)/$", ImageDetailView.as_view(), name="gallery.image"),
        )
