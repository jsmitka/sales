# -*- coding: utf-8 -*-

from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse

from sales.gallery.conf import settings
from sales.multimedia.models import ImageFile


class GallerySitemap(Sitemap):
    '''
    Sitemapa pro galerii.
    '''
    priority = 0.6
    changefreq = "never"

    def items(self):
        return ImageFile.objects.filter(tags__name=settings.GALLERY_TAG)

    def location(self, obj):
        return reverse("gallery.image", args=[obj.pk])
