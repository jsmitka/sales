# -*- coding: utf-8 -*-


from markdown.extensions.wikilinks import build_url
from django.template.defaultfilters import slugify


def build_slug_url(label, base, end):
    return build_url(slugify(label), base, end)
