# -*- coding: utf-8 -*-
"""
"""
import re

from django.conf import settings
from django.utils.encoding import smart_str, force_unicode
from django.utils.safestring import mark_safe


def transform(value, markup=None, *args, **kwargs):
    """
    Funkce transformuje vstup v podobe urciteho znackovaciho
    jazyka (markdown,...) na html.

    value      --  vstupni text (napr. v markdown)
    markup  --  v jakem znackovacim jazyku je vstup value
    """

    if markup is None:
        markup = settings.SALES_MARKUP_ENGINE

    f = globals()[markup]
    return f(value, *args, **kwargs)


def markdown(value, *args, **kwargs):
    try:
        import markdown
    except ImportError, err:
        if settings.DEBUG:
            raise err
        return force_unicode(value)
    else:
        if 'toc' in settings.SALES_MARKDOWN_EXTENSIONS:
            value = u'[TOC]\n\n%s' % value

        ext_cfg = kwargs.pop('extension_configs', {})
        ext_cfg.update(settings.SALES_MARKDOWN_EXTENSIONS_CONFIG)

        if 'wikilinks' in settings.SALES_MARKDOWN_EXTENSIONS:
            from md.extensions import build_slug_url
            wikil = ext_cfg.pop('wikilinks', [])
            wikil.append(('build_url', build_slug_url))
            ext_cfg['wikilinks'] = wikil

        kwargs['extension_configs'] = ext_cfg


        # Zjisti, jestli je v parametrech klicove slovo 'extensions'
        #TODO: Bylo by vhodne zjistit, jestli se nenachazi tez v args
        ext = kwargs.pop('extensions', [])
        # Pripoji rozsireni z nastaveni
        #TODO: Neresi duplicity
        ext = ext + list(getattr(settings, 'SALES_MARKDOWN_EXTENSIONS', []))
        # Ulozi zpet do slovniku
        kwargs['extensions'] = ext


        html = markdown.markdown(value, *args, **kwargs)

        # Hledani TOC a rozdeleni na toc a body
        regex = re.compile(r'\A(?P<toc><div class="toc">.*?</div>|<div class="toc" />)\n*(?P<body>.*)\Z', re.UNICODE+re.MULTILINE+re.DOTALL)
        result = regex.match(html).groupdict()

        #FIXME: Musi byt nastaveno generovani divu 'toc' a jeste se tak musi presne jmenovat.
        if result['toc'] == '<div class="toc"></div>':
            result['toc'] = ''

        return result

def restructuredtext(value, *args, **kwargs):
    """
    """
    pass


def untouched(value, *args, **kwargs):
    return {'body': force_unicode(value)}
