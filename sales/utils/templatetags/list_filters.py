# -*- coding: utf-8 -*-

import copy

from django import template
from django.utils.encoding import force_unicode

register = template.Library()


###############################################################################
#TODO: Vyresit pridani a odebrani prvku rychleji a usporneji.
# Funkce pro spojovani a odebirani prvku ze seznamu nejsou zrovna dobre
# navrzeny. Z hodnoty se nejdriv udela list (zde je to kvuli querysetu)
# a pak je prvek bud vyrazen nebo pridan, takze pri velkem mnozstvi pozadavku
# na navraceni pole s odebranou/pridanou hodnotou, se budu vytvaret
# velke mnozstvi seznamu a to neni zrovna rychle a usporne.
###############################################################################

@register.filter
def append(value, arg):
    val = list(value)
    try:
        val.append(arg)
    except AttributeError:
        pass

    return val


@register.filter
def remove(value, arg):
    val = list(value)
    try:
        val.remove(arg)
    except AttributeError:
        pass

    return val


@register.filter
def sort(value):
    try:
        value = map(lambda x: x[1], sorted([(force_unicode(item), item) for item in value]))
    except AttributeError:
        pass

    return value
