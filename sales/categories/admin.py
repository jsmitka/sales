# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.contenttypes import generic
from sorl.thumbnail.admin import AdminImageMixin

from forms import CategoryAdminForm
from models import Category, Categories

class CategoriesInline(generic.GenericTabularInline):
    model = Categories

class CategoryAdmin(AdminImageMixin, admin.ModelAdmin):
    form = CategoryAdminForm
    prepopulated_fields = {
            'url': ('name',)
    }
    list_display = ("name", "url", "url_full")

class CategoriesAdmin(admin.ModelAdmin):
    pass

admin.site.register(Category, CategoryAdmin)
admin.site.register(Categories, CategoriesAdmin)
