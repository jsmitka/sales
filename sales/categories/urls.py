# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *

from views import show, index

urlpatterns = patterns('',
    # Vypis clanku
    url(r'^$', index),
    #url(r'^(?P<url_name>[-_a-zA-Z0-9]{3,})/$', show, name="categories-show"),
    url(r'^(?P<url>/([-_a-zA-Z0-9]{3,}/)+)$', records, name="categories-show"),
)
