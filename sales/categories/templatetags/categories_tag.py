# -*- coding: utf-8 -*-

from django import template
from django.template.loader import get_template
from django.template import Context
from django.template import Library
from django.template import RequestContext
from django.template import resolve_variable

from sales.categories.models import Category,Categories


register = Library()

@register.inclusion_tag("categories/snippets/menu_head.html", takes_context=True)
def menu_head(context):
    """
    Render node.
    """

    menu = Category.roots.get_roots()
    context.update({"menu": menu})
    return context

@register.inclusion_tag("categories/snippets/features.html", takes_context=True)
def features(context, category_url=None):
    """
    Features
    """

    if category_url:
        cat = Category.objects.get(url=category_url)
        categories = Category.objects.filter(parent=cat)
    else:
        #categories = Category.roots.get_roots()
        categories = Category.roots.get_roots().order_by("?")[:6]
    
    ftrs = dict()
    #FIX: Opet sestavujici cyklus a v sablone vypisujici cyklus! SPATNE!!!
    for cat in categories:
        #Pripraveno na budouci pouziti
        #category_objects = Categories.objects.filter(category=cat)[:2]
        #if category_objects:
        #    ftrs[cat] = category_objects
        ftrs[cat] = Categories.objects.filter(category=cat)[:2]

    context.update({"subcategory": ftrs})
    return context
