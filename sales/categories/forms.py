# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext

from sales.generic_forms.forms import MarkdownTextarea


class CategoryAdminForm(forms.ModelForm):
    """
    This class represents form for message admin.
    """
    description = forms.CharField(widget=MarkdownTextarea(attrs={'class':'markdown_editor', 
                                                                  'rows': '10', 
                                                                  'cols': '60'}),
                                                           label=ugettext(u"Popis"),
                                                           required=False)

