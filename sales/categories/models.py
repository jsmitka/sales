# -*- coding: utf-8 -*-

from markdown import markdown

from django.db import models
from django.utils.translation import ugettext
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from sorl.thumbnail  import ImageField

# Create your models here.

class CategoryManager(models.Manager):
    """
    This class represents manager for categories
    """

    def get_roots(self):
        """
        Vrati seznam kategorii, ktere nemaji predka
        """
        return self.get_query_set().filter(parent__isnull=True).order_by("weight")


class Category(models.Model):
    name = models.CharField(verbose_name=ugettext(u"Kategorie"), 
                            max_length=255);
    url = models.SlugField(verbose_name=ugettext(u"URL"))
    url_full = models.CharField(verbose_name=ugettext(u"Plné URL"),
                                max_length=255, 
                                editable=False)
    description = models.TextField(verbose_name=ugettext(u"Popis"),
                                   blank=True, null=True)
    description_html = models.TextField(editable=False, blank=True, null=True)
    weight = models.PositiveIntegerField(verbose_name=ugettext(u"Pořadí"), 
                                         default=1)
    parent = models.ForeignKey("self", 
                               verbose_name=ugettext(u"Rodičovská kategorie"),
                               default=0, blank=True, null=True)
    category_image = ImageField(verbose_name=ugettext(u"Obrázek kategorie"), blank=True, null=True, upload_to="imgs/")
    template = models.CharField(verbose_name=ugettext(u"Šablona kategorie"), max_length=255, blank=True, null=True)

    roots = CategoryManager()
    objects = models.Manager()
  
    def get_children(self):
        """
        Fce vrati seznam potomku
        """
        return self._default_manager.filter(parent=self).order_by("weight")

    def get_parents(self):
        """
        Generator - prochazi stromem a vraci predky.
        """
        cat = self
        while cat is not None:
            yield cat
            cat = cat.parent

    def get_flat_tree(self):
        """
        Fce vrati strom kategorii jako seznam
        """
        tree = []
        cats = self.get_parents()

        for cat in cats:
            tree.insert(0, cat)

        return tree

    @models.permalink
    def get_absolute_url(self):
        """
        Return absolute url
        """
        return ("categories-show", (self.url_full,))
    
    def save(self):
        # Generovani cele url tedy obsazeni i nadrazenych kategorii
        #FIXME: url_full se prepocitava pri kazdem ulozeni.
        self.url_full = ""
        cats = self.get_parents()
        for cat in cats:
            self.url_full = "/".join([cat.url, self.url_full])
        self.url_full = "".join(["/", self.url_full])

        if self.description:
            self.description_html = markdown(self.description)
        super(Category, self).save()

    def __unicode__(self):
        tree = self.get_flat_tree()
        tree_txt = ""

        for item in tree:
            if tree_txt == "":
                tree_txt = item.name
            else:
                tree_txt = " > ".join([tree_txt, item.name])

        return tree_txt
  
    def __str__(self):
        return self.__unicode__()
  
    class Meta:
        verbose_name = ugettext(u"Kategorie");
        verbose_name_plural = ugettext(u"Kategorie");


class CategoriesManager(models.Manager):
    """
    This class represents manager for categories
    """
    def get_objects_for_models(self, model):
        """
        Vrati objekty zadaneho modelu
        
        model -- model
        """
        return self.get_query_set().filter(content_type=ContentType.objects.get_for_model(model))

    def get_category_objects_for_models(self, category, model):
        """
        Vrati objekty zadane kategorie a zadaneho modelu
        
        category -- kategorie
        model -- model
        """
        return self.get_objects_for_models(model).filter(category=cat)


class Categories(models.Model):
    """
    This class represents categories and generic relations.
    """
    # Pole pro svazani s jakymkoliv objektem
    category = models.ForeignKey("categories.Category", verbose_name=ugettext(u"Kategorie"))
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    
    objects = CategoriesManager()

    def __unicode__(self):
        return self.category.name

    class Meta:
        verbose_name = ugettext(u"Spojení kategorie s objekty");
        verbose_name_plural = ugettext(u"Spojení kategorií s objekty");
