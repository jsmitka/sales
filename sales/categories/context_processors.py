# -*- coding: utf-8 -*-
"""
Kontext procesor kategorii - napr. vraci barvu stranek.
"""

__version__ = ""

from django.conf import settings

from models import Category


#TODO: Predelat na fungovani z procesoru
def site_color(request):
    """
    Vrati aktualni barvu stranek.
    
    request -- request
    """
    path = request.path[1:-1].split("/")

    #FIX: Natvrdo zadratovane a jeste to bere jen kde je nastaven styl.
    if path[0] == "categories":
        try:
            cat = Category.objects.get(url=path[1])
        except IndexError:
            return {"SITE_COLOR": "red"}

        return {"SITE_COLOR": cat.get_style()}
    else:
        return {"SITE_COLOR": "red"}
