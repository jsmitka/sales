# -*- coding: utf-8 -*-
# Create your views here.

from django.shortcuts import get_object_or_404, get_list_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from models import Category, Categories

def index(request, template="categories/index.html", extra_context=None):
    """
    
    request -- request
    template -- template
    extra_context -- extra_context
    """
    cat = Category.roots.get_roots()
    return render_to_response(template,
                              {'categories': cat,},
                              context_instance=RequestContext(request))

def show(request,
         url_name,
         template="categories/list.html",
         extra_context=None):
    """
    Zobrazi seznam objektu anebo seznam podkategorii
    
    request -- request
    url_name -- jedinecne url
    """
    cat = Category.objects.get(url_full=url_name)
    childs = cat.get_children()

    if cat.template:
        template = cat.template


    return render_to_response(template,
                              {'category': cat,
                               'categories': childs,},
                              context_instance=RequestContext(request))
