# -*- coding: utf-8 -*-

import datetime

from django.contrib.sitemaps import Sitemap

from models import NewsMessage


class NewsMessageSitemap(Sitemap):
    '''
    Sitemapa pro novinky.
    '''
    priority = 0.6
    changefreq = "never"

    def items(self):
        return NewsMessage.objects.filter(valid_from__lte=datetime.date.today()).order_by("-valid_from", "-pk")

    def lastmod(self, obj):
        return obj.modified
