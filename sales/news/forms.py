# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext

from sales.generic_forms.forms import MarkdownTextarea


class NewsMessageAdminForm(forms.ModelForm):
    """
    This class represents form for message admin.
    """
    news_message = forms.CharField(widget=MarkdownTextarea(attrs={'class':'markdown_editor', 
                                                                  'rows': '10', 
                                                                  'cols': '60'}),
                                                           label=ugettext(u"Zpráva"))
