# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Adding model 'NewsMessage'
        db.create_table('news_newsmessage', (
            ('valid_from', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('url', self.gf('django.db.models.fields.SlugField')(max_length=50, db_index=True)),
            ('news_message', self.gf('django.db.models.fields.TextField')()),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('news_message_html', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('news', ['NewsMessage'])

        # Adding M2M table for field news_message_groups on 'NewsMessage'
        db.create_table('news_newsmessage_news_message_groups', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('newsmessage', models.ForeignKey(orm['news.newsmessage'], null=False)),
            ('newsmessagegroups', models.ForeignKey(orm['news.newsmessagegroups'], null=False))
        ))
        db.create_unique('news_newsmessage_news_message_groups', ['newsmessage_id', 'newsmessagegroups_id'])

        # Adding model 'NewsMessageGroups'
        db.create_table('news_newsmessagegroups', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('news', ['NewsMessageGroups'])

        # Adding M2M table for field email_users on 'NewsMessageGroups'
        db.create_table('news_newsmessagegroups_email_users', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('newsmessagegroups', models.ForeignKey(orm['news.newsmessagegroups'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('news_newsmessagegroups_email_users', ['newsmessagegroups_id', 'user_id'])

        # Adding model 'NewsMessageSentStatus'
        db.create_table('news_newsmessagesentstatus', (
            ('status', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('attempt', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('news_message', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['news.NewsMessage'])),
            ('attempt_last', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('error', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('news', ['NewsMessageSentStatus'])
    
    
    def backwards(self, orm):
        
        # Deleting model 'NewsMessage'
        db.delete_table('news_newsmessage')

        # Removing M2M table for field news_message_groups on 'NewsMessage'
        db.delete_table('news_newsmessage_news_message_groups')

        # Deleting model 'NewsMessageGroups'
        db.delete_table('news_newsmessagegroups')

        # Removing M2M table for field email_users on 'NewsMessageGroups'
        db.delete_table('news_newsmessagegroups_email_users')

        # Deleting model 'NewsMessageSentStatus'
        db.delete_table('news_newsmessagesentstatus')
    
    
    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'blank': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'news.newsmessage': {
            'Meta': {'object_name': 'NewsMessage'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'news_message': ('django.db.models.fields.TextField', [], {}),
            'news_message_groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['news.NewsMessageGroups']", 'null': 'True', 'blank': 'True'}),
            'news_message_html': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'valid_from': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'})
        },
        'news.newsmessagegroups': {
            'Meta': {'object_name': 'NewsMessageGroups'},
            'email_users': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'news.newsmessagesentstatus': {
            'Meta': {'object_name': 'NewsMessageSentStatus'},
            'attempt': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'attempt_last': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'error': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'news_message': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['news.NewsMessage']"}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        }
    }
    
    complete_apps = ['news']
