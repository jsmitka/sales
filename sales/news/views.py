# -*- coding: utf-8 -*-

import datetime

from django.conf import settings
from django.views.generic import ListView, DateDetailView

from models import NewsMessage


class NewsListView(ListView):
    model = NewsMessage
    queryset = NewsMessage.objects.filter(valid_from__lte=datetime.date.today()).order_by("-valid_from", "-pk")
    paginate_by = getattr(settings, "SALES_NEWS_LIST_PAGINATED_BY", None)
    allow_empty = getattr(settings, "SALES_NEWS_LIST_ALLOW_EMPTY", False)


class NewsDetailView(DateDetailView):
    model = NewsMessage
    queryset = NewsMessage.objects.all().order_by("-valid_from", "-pk")
    slug_field = "url"
    date_field = "valid_from"
    allow_future = True
    month_format = "%m"
