# -*- coding: utf-8 -*-

import datetime

from django.template import Library

from sales.news.models import NewsMessage



register = Library()

@register.inclusion_tag("news/snippets/news.html", takes_context=True)
def get_news(context, count=5):
    news = NewsMessage.objects.filter(valid_from__lte=datetime.date.today()).order_by("-valid_from", "-pk")[:count]
    context.update({"news": news})
    return context
