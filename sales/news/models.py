# -*- coding: utf-8 -*-
import datetime

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext
from django.template.defaultfilters import slugify

from sales.utils.markup import transform


class NewsMessage(models.Model):
    """
    This class represents message for subscribers.
    """
    title = models.CharField(verbose_name=ugettext(u"Titulek"),
                             max_length=255)
    url = models.SlugField(verbose_name=ugettext(u"URL"))
    perex = models.CharField(verbose_name=ugettext(u"Perex"),
                             max_length=140, blank=True, null=True)
    news_message = models.TextField(verbose_name=ugettext(u"Zpráva"))
    news_message_html = models.TextField(verbose_name=ugettext(u"Zpráva"),
                                    editable=False, blank=True, null=True)
    # Pouziti hodnoty default je kvuli predvyplneni uzivatelem,
    # ktery muze hodnotu zmenit (zmeni kdy je newska aktualni),
    # a proto nenipouzito "auto_now" nebo "auto_now_add".
    valid_from = models.DateField(verbose_name=ugettext(u"Platný od"),
                                  default=datetime.date.today)
    created = models.DateTimeField(verbose_name=ugettext(u"Vytvořeno"),
                                   auto_now_add=True, editable=False)
    modified = models.DateTimeField(verbose_name=ugettext(u"Upraveno"),
                                    auto_now=True, editable=False)
    news_message_groups = models.ManyToManyField("news.NewsMessageGroups",
                                            verbose_name=ugettext(u"Skupiny"),
                                            blank=True, null=True)

    def save(self):
        self.news_message_html = transform(self.news_message, safe_mode="replace")['body']

        if not self.url:
            self.url = slugify(self.title)

        super(NewsMessage, self).save()

    @models.permalink
    def get_absolute_url(self):
        return ("news-message",
                (),
                {"year": str(self.valid_from.year),
                 "month": str(self.valid_from.month),
                 "day": str(self.valid_from.day),
                 "slug": str(self.url)})

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = ugettext(u"Zpráva")
        verbose_name_plural = ugettext(u"Zprávy")


class NewsMessageGroups(models.Model):
    """
    This class represents user groups.
    """
    name = models.CharField(verbose_name=ugettext(u"Název"),
                            max_length=255)
    email_users = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                         verbose_name=ugettext(u"Uživatelé"))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = ugettext(u"Skupina")
        verbose_name_plural = ugettext(u"Skupiny")


class NewsMessageSentStatus(models.Model):
    """
    This class represents status of sent messages.
    """
    OK_STATUS = 1
    ERROR_STATUS = 0
    STATUS_CHOICES = (
            (OK_STATUS, ugettext(u"OK")),
            (ERROR_STATUS, ugettext(u"ERROR")),
    )

    news_message = models.ForeignKey(NewsMessage, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False)
    attempt_last = models.DateTimeField(verbose_name=ugettext(u"Vytvořeno"),
                                        auto_now=True, editable=False)
    attempt = models.SmallIntegerField(verbose_name=ugettext(u"Pokus č."),
                                       default=0)
    status = models.SmallIntegerField(verbose_name=ugettext(u"Status"),
                                      choices=STATUS_CHOICES)
    error = models.CharField(verbose_name=ugettext(u"Chyba"),
                             max_length=255,
                             editable=False,
                             blank=True,
                             null=True)

    def __unicode__(self):
        #FIXME: vypisovat uzivateluv email
        return u"%s %s %s %s" % (self.news_message, self.user, self.status, self.attempt_last)

    class Meta:
        verbose_name = ugettext(u"Status zaslané zprávy")
        verbose_name_plural = ugettext(u"Status zaslaných zpráv")
