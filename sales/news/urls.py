# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

from .views import NewsListView, NewsDetailView


urlpatterns = patterns('',
     url(r'^$',
         NewsListView.as_view(),
         name="news-list"),

     url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<slug>[-\w]+)/$',
         NewsDetailView.as_view(),
         name="news-message"),
     )
