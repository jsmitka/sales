# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext

from models import NewsMessage, NewsMessageGroups, NewsMessageSentStatus
from forms import NewsMessageAdminForm


class NewsMessageAdmin(admin.ModelAdmin):
    """
    This class represents message admin.
    """
    form = NewsMessageAdminForm
    prepopulated_fields = {
        'url': ('title',)
    }

    list_display = ("title", "valid_from", "created",)

    actions = ['sendmail']

    def sendmail(self, request, queryset):
        for obj in queryset:
            pass
    sendmail.short_description = ugettext(u"Odeslat vybrané novinky")

admin.site.register(NewsMessage, NewsMessageAdmin)


class NewsMessageGroupsAdmin(admin.ModelAdmin):
    """
    This class represents message groups admin.
    """
    pass

admin.site.register(NewsMessageGroups, NewsMessageGroupsAdmin)


class NewsMessageSentStatusAdmin(admin.ModelAdmin):
    """
    This class represents message sent status admin.
    """
    pass

admin.site.register(NewsMessageSentStatus, NewsMessageSentStatusAdmin)
