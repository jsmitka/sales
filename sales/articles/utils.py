# -*- coding: utf-8 -*-


def rstrip(s, ch):
    """
    Funkce odstrani z konce retezce 's' znak 'ch',
    pokud tam je, pokud ne, tak se nic neprovede.
    """
    if len(s) > 0 and s[-1] == ch:
        s = s[:-1]
    return s


def lstrip(s, ch):
    '''
    Funkce odstrani ze zacatku retezce 's' znak 'ch',
    pokud tam je, pokud ne, tak se nic neprovede.
    '''
    if len(s) > 0 and s[0] == ch:
        s = s[1:]
    return s


def strip(s, ch):
    """
    Funkce odstrani znak 'ch' ze zacatku a konce retezce 's'.
    """
    return rstrip(lstrip(s, ch), ch)

