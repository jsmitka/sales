# -*- coding: utf-8 -*-

from django.contrib.sitemaps import Sitemap

from models import Article


class ArticlesSitemap(Sitemap):
    '''
    Sitemapa pro clanky.
    '''
    priority = 0.6
    changefreq = "weekly"

    def items(self):
        return Article.objects.all()

    def lastmod(self, obj):
        return obj.modified

