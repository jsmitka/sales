# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, get_list_or_404
from django.contrib.auth.decorators import login_required
#from django.contrib.auth.decorators import permission_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
#from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.core.urlresolvers import reverse
from django.views.decorators.cache import cache_control
from django.views.decorators.cache import never_cache

from sales.generic_forms.forms import DeleteForm

#from conf import settings
from forms import ArticleEditForm
from models import Article
from utils import strip


def index(request, template='articles/index.html', extra_context=None):
    try:
        articles = Article.objects.all().order_by('title')
    except Article.DoesNotExist:
        articles = None

    return render_to_response(template,
                              {'articles': articles},
                              context_instance=RequestContext(request))


def article(request, url, template="articles/article.html", extra_context=None):
    """
    Show article.
    """
    article = get_object_or_404(Article, url=url)
    return render_to_response(template,
                              {"article": article},
                              context_instance=RequestContext(request))

@never_cache
def page_not_exist(request,
                   url,
                   template="articles/page-not-exist.html",
                   extra_context=None):

    url = strip(url, '/')

    return render_to_response(template,
                              {"url": url},
                              context_instance=RequestContext(request))


@login_required
@never_cache
def articles_list(request, template="articles/user_list.html", extra_context=None):
    try:
        articles = Article.objects.get_all().filter(user=request.user)
    except Article.DoesNotExist:
        articles = None

    return render_to_response(template,
                              {'articles': articles},
                              context_instance=RequestContext(request))

@login_required
@never_cache
def edit(request, url=None, template="articles/edit.html", extra_context=None):
    if url is not None:
        # Odstraneni pocatecniho a koncoveho lomitka
        #TODO: Asi by bylo lepsi, kdyby lomitka zustali
        #TODO: a upravil se jen spravne model.

        url = strip(url, '/')

        try:
            article = Article.objects.get_all().get(url=url)
        except Article.DoesNotExist:
            article = Article(url=url)
    else:
        article = None

    if request.method == 'POST':
        form = ArticleEditForm(request.POST,
                               request.FILES or None,
                               instance=article)
        if form.is_valid():
            article = form.save(commit=False)
            article.user = request.user
            article.save()
            return HttpResponseRedirect(article.get_absolute_url())
    else:
        form = ArticleEditForm(instance=article)

    return render_to_response(template,
                              {"form": form},
                              context_instance=RequestContext(request))

@login_required
@never_cache
def delete(request, url, template="articles/delete.html", extra_context=None):
    '''
    Pohled pro vymazani clanku.
    '''
    article = get_object_or_404(Article, url=url)

    if request.method == "POST":
        delete_form = DeleteForm(request.POST)
        if delete_form.is_valid():
            article.delete()
        return HttpResponseRedirect(reverse("article-list"))
    else:
        delete_form = DeleteForm()

    return render_to_response(template,
                              {"form": delete_form, "object": article},
                              context_instance=RequestContext(request))
