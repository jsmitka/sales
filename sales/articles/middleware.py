# -*- coding: utf-8 -*-

import re

from django.http import Http404

from views import page_not_exist
from conf import settings
from utils import lstrip


#TODO: Lepsi pristup pro ziskani spravneho url,
#TODO: kde je tento middleware zavesen.
#
# Idea: Importovat jednu funkci z views modulu, pak ziskat url pomoci reverse,
#       nacist modul urls a ziskat pattern funkce, pak jen staci
#       odecist url ziskane z reverse a z patternu a mel by zustat jen prefix
#       pro clanky.

class ArticleFallbackMiddleware(object):
    """
    Middleware zobrazi stranku pro zalozeni noveho clanku, kdyz neexistuje.
    """

    def __init__(self):
        # Pokud je nastaven prefix url pro clanky,
        # tak se pouzije pro hledani cesty v url.
        if settings.ARTICLE_URL_PREFIX:
            self.url_prefix = re.compile(r'^%(articles_url)s/' % {'articles_url': settings.ARTICLE_URL_PREFIX})
        else:
        # Rozdeli sales.articles.middleware na dva retezce
        # a to sales.articles a middleware
            app, module = __name__.rsplit(".", 1)

            # Nacteni spravneho modulu s urlpatterns
            urlp = __import__(settings.settings.ROOT_URLCONF)

            # Vyhledavani cesty (prefixu pro clanky) v urlpatterns 
            for urlpattern in urlp.urls.urlpatterns:
                app_url_module = getattr(urlpattern, 'urlconf_name', None)
                # Pokud pattern ukazuje na modul app.urls (napr. sales.articles.urls),
                # tedy na urls.py, ktere je obsazene v balikcku s timto middlewarem,
                # tak ulozi jeho regex a pouzije ho ve vyhledavani cesty, kdy se ma tento
                # middleware uplatnit.
                if app_url_module == (app + '.urls'):
                    self.url_prefix = urlpattern.regex
                    # Nasli jsme, tak uz nema cenu hledat dal.
                    break


    def process_response(self, request, response):
        if response.status_code != 404:
            return response # No need to check for a flatpage for non-404 responses.

        path = lstrip(request.path, '/')
        url_match = self.url_prefix.match(path)

        if url_match:
            try:
                return page_not_exist(request, path[url_match.end():])
            except Http404:
                return response
            except:
                # Pristup ke globalnimu nastaveni
                if settings.settings.DEBUG:
                    raise
                return response
        else:
            return response

