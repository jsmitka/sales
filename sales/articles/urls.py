# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from views import article, edit, index, articles_list, delete


urlpatterns = patterns('',
        #url(r'^(?P<url>[\w-]+)-(?P<id>\d+)/', article, name="article-show"),
        #url(r'^category(?P<url>/([-_a-zA-Z0-9]{3,}/)+)$', records, name="categories-show"),
        url(r'^$', index, name="article-index"),
        url(r'^new/$', edit, name="article-new"),
        url(r'^list/$', articles_list, name="article-list"),
        url(r'^(?P<url>[\w-]+)/edit/', edit, name="article-edit"),
        url(r'^(?P<url>[\w-]+)/delete/', delete, name="article-delete"),
        url(r'^(?P<url>[\w-]+)/', article, name="article-show"),
        )
