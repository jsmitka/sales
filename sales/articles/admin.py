# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext

from models import Article
from forms import ArticleAdminForm


class ArticleAdmin(admin.ModelAdmin):
    """
    This class represents message admin.
    """
    form = ArticleAdminForm
    prepopulated_fields = {
        'url': ('title',)
    }

    list_display = ("title", "pub_date", "created", "published",)

    def queryset(self, request):
        return self.model._default_manager.get_all()

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

admin.site.register(Article, ArticleAdmin)



