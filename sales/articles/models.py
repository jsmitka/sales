# -*- coding: utf-8 -*-
import datetime

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.template.defaultfilters import slugify
from sorl.thumbnail import ImageField

from sales.utils.markup import transform

from conf import settings


class ArticleManager(models.Manager):
    def get_query_set(self):
        return super(self.__class__, self).get_query_set().filter(published=True, status=Article.LIVE_STATUS).order_by("-pub_date")

    def get_all(self):
        return super(self.__class__, self).get_query_set().order_by("-pub_date")


class Article(models.Model):
    LIVE_STATUS = 1
    DRAFT_STATUS = 2
    STATUS_CHOICES = (
            (LIVE_STATUS, _(u"Kompletní")),
            (DRAFT_STATUS, _(u"Rozepsáno")),
            )

    user = models.ForeignKey(User, editable=False)
    title = models.CharField(verbose_name=_(u"Titulek"), max_length=255)
    url = models.SlugField(verbose_name=_(u"URL"), unique=True)
    image = ImageField(verbose_name=_(u"Obrázek článku"),
                       upload_to=settings.ARTICLE_IMAGE_UPLOAD_PATH,
                       blank=True,
                       null=True)
    status = models.SmallIntegerField(verbose_name=_(u"Status"),
                                      choices=STATUS_CHOICES, default=DRAFT_STATUS)
    perex = models.CharField(verbose_name=_(u"Perex"), max_length=140,
                             blank=True, null=True)
    article = models.TextField(verbose_name=_(u"Článek"))
    article_html = models.TextField(verbose_name=_(u"Článek"),
                                    editable=False, blank=True, null=True)
    menu_html = models.TextField(verbose_name=_(u'Obsah (menu)'),
                                 blank=True,
                                 null=True)
    pub_date = models.DateField(verbose_name=_(u"Publikovat dne"),
                                  default=datetime.date.today)
    gen_toc = models.BooleanField(verbose_name=_(u"Vytvořit automaticky menu pro článek"),
                                    default=settings.ARTICLE_GENERATE_TOC)
    published = models.BooleanField(verbose_name=_(u"Publikovat"),
                                    default=settings.ARTICLE_AUTO_PUBLISH)
    created = models.DateTimeField(verbose_name=_(u"Vytvořeno"),
                                   auto_now_add=True, editable=False)
    modified = models.DateTimeField(verbose_name=_(u"Upraveno"),
                                    auto_now=True, editable=False)

    objects = ArticleManager()

    def save(self):
        article_html = transform(self.article, safe_mode=settings.ARTICLE_MARKDOWN_DEFAULT_BEHAVIOR)
        self.article_html = article_html['body']

        if self.gen_toc:
            self.menu_html = article_html['toc']

        if not self.url:
            self.url = slugify(self.title)

        super(self.__class__, self).save()

    @models.permalink
    def get_absolute_url(self):
        return ("article-show",
                (),
                {"url": str(self.url)})

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _(u"Článek")
        verbose_name_plural = _(u"Články")


