# -*- coding: utf-8 -*-
from django.conf import settings

ARTICLE_AUTO_PUBLISH = getattr(settings, "ARTICLE_AUTO_PUBLISH", False)

ARTICLE_URL_PREFIX = getattr(settings,
                             "ARTICLE_URL_PREFIX",
                             None)

ARTICLE_GENERATE_TOC = getattr(settings,
                           "ARTICLE_GENERATE_TOC",
                           True)

ARTICLE_MARKDOWN_DEFAULT_BEHAVIOR = getattr(settings,
                                            "ARTICLE_MARKDOWN_DEFAULT_BEHAVIOR",
                                            "escape")

ARTICLE_IMAGE_UPLOAD_PATH = getattr(settings,
                                    "ARTICLE_IMAGE_UPLOAD_PATH",
                                    "images")
