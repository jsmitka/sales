# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext

from sales.generic_forms.forms import MarkdownTextarea
from models import Article


class ArticleAdminForm(forms.ModelForm):
    """
    This class represents form for message admin.
    """
    article = forms.CharField(widget=MarkdownTextarea(attrs={'class':'markdown_editor', 
                                                                  'rows': '10', 
                                                                  'cols': '60'}),
                                                           label=ugettext(u"Článek"))

    
#TODO: Oba formulare jsou si hodne podobne, vymyslet jak udelat jen jeden a zbytek podedit.
class ArticleEditForm(forms.ModelForm):
    """
    This class represents form for message admin.
    """
    article = forms.CharField(widget=MarkdownTextarea(attrs={'class':'markdown_editor', 
                                                                  'rows': '10', 
                                                                  'cols': '60'}),
                                                           label=ugettext(u"Článek"))

    class Meta:
        model = Article

