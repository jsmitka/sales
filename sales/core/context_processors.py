# -*- coding: utf-8 -*-
"""
    Modul s uzitecnymi funkcemi, co naplni kontext. 
    Napr. pristup sablony nastaveni.

"""

__version__ = ""

from django.conf import settings

def revision(request):
    """
    Context processor - cislo revize.

    request -- http pozadavek, zde neni potreba
    """
    return {'REVISION': settings.PROJECT_REVISION}
