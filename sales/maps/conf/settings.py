# -*- coding: utf-8 -*-
from django.conf import settings


SALES_MAPS_SERVICE_URL = getattr(settings,
                         "SALES_MAPS_URL",
                         "http://maps.googleapis.com/maps/api/staticmap")

SALES_MAPS_SIZE = getattr(settings,
                         "SALES_MAPS_SIZE",
                         "400x300")

SALES_MAPS_ZOOM = getattr(settings,
                          "SALES_MAPS_ZOOM",
                          "12")

SALES_MAPS_URL = getattr(settings,
                         "SALES_MAPS_URL",
                         "%(url)s?center=%(location)s&amp;zoom=%(zoom)s&amp;size=%(size)s&amp;maptype=roadmap&amp;sensor=false&amp;format=png&amp;markers=size:mid%%7Ccolor:red%%7C%(location)s")
