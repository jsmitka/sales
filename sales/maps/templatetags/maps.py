# -*- coding: utf-8 -*-
from django import template
from django.template import Context
from django.template import Library
from django.utils.http import urlquote_plus

from sales.maps.conf import settings


register = Library()


@register.simple_tag
def static_map(location, size=None, alt="Mapa"):
    """
    Vrati mapu jako obrazek (viz. http://http://code.google.com/intl/cs-CZ/apis/maps/documentation/staticmaps/)
    """

    url = settings.SALES_MAPS_URL % {"url": settings.SALES_MAPS_SERVICE_URL,
                                     "location": urlquote_plus(location),
                                     "zoom": settings.SALES_MAPS_ZOOM,
                                     "size": size or settings.SALES_MAPS_SIZE}

    return "<img class=\"google_map\" src=\"%s\" alt=\"%s\" />" % (url, alt)
