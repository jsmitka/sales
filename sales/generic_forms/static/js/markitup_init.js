$(document).ready(function()    {
    mySettings.resizeHandle = false;
    mySettings.previewInWindow = 'width=800, height=600, resizable=yes, scrollbars=yes';

    // Pro spravnou funkci musi byt nejdrive nahran image_selector.
    mySettings.markupSet[13] = {name:'Picture', 
                                key:'P', 
                                //beforeInsert: ImageSelector.init,
                                beforeInsert: function(){$('<iframe src="/multimedia/images/"></iframe>').modal({dataCss: {'width': '850px', 'height': '700px'}})},
                                //replaceWith: ImageSelector.select,
                                //afterInsert: ImageSelector.destroy
                               };

    $('textarea.markdown_editor').markItUp(mySettings);
});
