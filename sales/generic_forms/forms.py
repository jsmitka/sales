# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext
from django.conf import settings


class DeleteForm(forms.Form):
    delete = forms.BooleanField(required=True, initial=True, widget=forms.HiddenInput)


class MarkdownTextarea(forms.Textarea):
    class Media:
        if settings.DEBUG:
            js = ("%sjs/jquery.js" % settings.STATIC_URL,
                  "%smarkitup/jquery.markitup.js" % settings.STATIC_URL,)
        else:
            js = ("%sjs/jquery.min.js" % settings.STATIC_URL,
                  "%smarkitup/jquery.markitup.pack.js" % settings.STATIC_URL,)

        js = js + (
            "%smarkitup/sets/markdown/set.js" % settings.STATIC_URL,
            "%sjs/jquery.simplemodal.js" % settings.STATIC_URL,
            "%sjs/markitup_init.js" % settings.STATIC_URL
        )

        css = {"screen": ("%smarkitup/skins/simple/style.css" % settings.STATIC_URL,
                          "%smarkitup/sets/markdown/style.css" % settings.STATIC_URL,
                          "%scss/modal.css" % settings.STATIC_URL,
                          "%scss/admin.css" % settings.STATIC_URL)}


class HtmlTextArea(forms.Textarea):
    """
    This class represents html textarea
    """
    class Media:
        if settings.DEBUG:
            js = ("%sjs/jquery.js" % settings.STATIC_URL,
                  "%swymeditor/jquery.wymeditor.js" % settings.STATIC_URL,)
        else:
            js = ("%sjs/jquery.min.js" % settings.STATIC_URL,
                  "%swymeditor/jquery.wymeditor.min.js" % settings.STATIC_URL,)

        js = js + (
            #"%sjs/jquery.simplemodal.js",
            "%sjs/markitup_init.js" % settings.STATIC_URL,
        )

        css = {
            "screen": (
                #"%scss/modal.css",
                "%scss/admin.css" % settings.STATIC_URL,
            )
        }
