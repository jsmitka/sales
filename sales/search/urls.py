# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from views import search

urlpatterns = patterns('',
        url(r'^$', search, name="search"),
        )
