# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext

from models import SearchStat


class SearchStatAdmin(admin.ModelAdmin):
    fields = ('search_term', 'search_count')
    list_display = ('search_term', 'search_count')
    ordering = ('-search_count', )

admin.site.register(SearchStat, SearchStatAdmin)
