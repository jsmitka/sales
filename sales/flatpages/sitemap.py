# -*- coding: utf-8 -*-

from django.contrib.sitemaps import Sitemap

from models import FlatPage


class FlatPageSitemap(Sitemap):
    '''
    Sitemapa pro stranky.
    '''
    priority = 0.6
    changefreq = "never"

    def items(self):
        return FlatPage.objects.all()
