# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import iri_to_uri
from django.core.urlresolvers import get_script_prefix

from sales.utils.markup import transform


class FlatPage(models.Model):
    url = models.CharField(_('URL'), max_length=100, db_index=True)
    title = models.CharField(_('title'), max_length=200)
    content = models.TextField(_('content'), blank=True)
    content_html = models.TextField(verbose_name=_(u"content"),
                                    editable=False, blank=True, null=True)
    enable_comments = models.BooleanField(_('enable comments'))
    template_name = models.CharField(
        _('template name'), max_length=70, blank=True,
        help_text=_("Example: 'flatpages/contact_page.html'."
                    "If this isn't provided, the system will use 'flatpages/default.html'."))
    registration_required = models.BooleanField(
        _('registration required'),
        help_text=_("If this is checked, only logged-in users will be able to view the page.")
    )
    sites = models.ManyToManyField(Site)

    def save(self):
        self.content_html = transform(self.content, safe_mode="replace")['body']
        super(FlatPage, self).save()

    class Meta:
        db_table = 'sales_flatpage'
        verbose_name = _('flat page')
        verbose_name_plural = _('flat pages')
        ordering = ('url',)

    def __unicode__(self):
        return u"%s -- %s" % (self.url, self.title)

    def __str__(self):
        return "%s -- %s" % (self.url, self.title)

    def get_absolute_url(self):
        return iri_to_uri(get_script_prefix().rstrip('/') + self.url)
