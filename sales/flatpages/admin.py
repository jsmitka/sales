# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin
from django.contrib.admin.sites import NotRegistered
from django.utils.translation import ugettext_lazy as _

from sales.flatpages.models import FlatPage
from sales.generic_forms.forms import MarkdownTextarea


class NewsMessageAdminForm(forms.ModelForm):
    """
    This class represents form for message admin.
    """


class FlatpageForm(forms.ModelForm):
    url = forms.RegexField(label=_("URL"), max_length=100, regex=r'^[-\w/\.~]+$',
        help_text = _("Example: '/about/contact/'. Make sure to have leading"
                      " and trailing slashes."),
        error_message = _("This value must contain only letters, numbers,"
                          " dots, underscores, dashes, slashes or tildes."))
    content = forms.CharField(widget=MarkdownTextarea(attrs={'class':'markdown_editor',
        'rows': '10',
        'cols': '60'}),
        label=_(u"content"))

    class Meta:
        model = FlatPage


class FlatPageAdmin(admin.ModelAdmin):
    form = FlatpageForm
    fieldsets = (
        (None, {'fields': ('url', 'title', 'content', 'sites')}),
        (_('Advanced options'), {'classes': ('collapse',), 'fields': ('enable_comments', 'registration_required', 'template_name')}),
    )
    list_display = ('url', 'title')
    list_filter = ('sites', 'enable_comments', 'registration_required')
    search_fields = ('url', 'title')

try:
    admin.site.unregister(FlatPage)  # XXX: quickfix - fungovani testu
except NotRegistered:
    pass

admin.site.register(FlatPage, FlatPageAdmin)
