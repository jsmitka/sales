from sales.flatpages.tests.csrf import *
from sales.flatpages.tests.forms import *
from sales.flatpages.tests.middleware import *
from sales.flatpages.tests.templatetags import *
from sales.flatpages.tests.views import *
