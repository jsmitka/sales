# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *

urlpatterns = patterns('sales.flatpages.views',
    (r'^(?P<url>.*)$', 'flatpage'),
)
