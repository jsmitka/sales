# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext

from sorl.thumbnail import ImageField


class Contact(models.Model):
    """
    This class represents contact.
    """

    name = models.CharField(verbose_name=ugettext(u"Jméno"),
                            help_text=ugettext(u"Např. jméno a příjmení nebo nazev oddělení vaší společnosti."),
                            max_length=255)
    email = models.EmailField(verbose_name=ugettext(u"Email"))
    master_image = ImageField(verbose_name=ugettext(u"Obrázek"),
                              blank=True, null=True,
                              upload_to="imgs/")
    description = models.TextField(verbose_name=ugettext(u"Popis"),
                                   blank=True,
                                   null=True)
    group = models.ManyToManyField("contact.ContactGroup",
                                   verbose_name=ugettext(u"Skupina"),
                                   blank=True,
                                   null=True)
    ico = models.CharField(verbose_name=ugettext(u"IČO"),
                           max_length=16,
                           blank=True,
                           null=True)
    dic = models.CharField(verbose_name=ugettext(u"DIČ"),
                           max_length=16,
                           blank=True,
                           null=True)
    telefon = models.CharField(verbose_name=ugettext(u"Telefon"),
                               max_length=16,
                               blank=True,
                               null=True)
    mobil = models.CharField(verbose_name=ugettext(u"Mobil"),
                             max_length=16,
                             blank=True,
                             null=True)
    fax = models.CharField(verbose_name=ugettext(u"Fax"),
                           max_length=16,
                           blank=True,
                           null=True)
    position = models.CharField(verbose_name=ugettext(u"Pozice"),
                                max_length=255,
                                blank=True,
                                null=True)
    address = models.CharField(verbose_name=ugettext(u"Adresa"),
                               max_length=255,
                               blank=True,
                               null=True)
    city = models.CharField(verbose_name=ugettext(u"Město"),
                            max_length=255,
                            blank=True,
                            null=True)
    country = models.CharField(verbose_name=ugettext(u"Stát"),
                               max_length=255,
                               blank=True,
                               null=True)
    zip_code = models.CharField(verbose_name=ugettext(u"PSČ"),
                                max_length=255,
                                blank=True,
                                null=True)
    public_profile = models.BooleanField(verbose_name=ugettext(u'Zveřejnit profil'),
                                         default=True)
    public_email = models.BooleanField(verbose_name=ugettext(u'Povolit odeslání emailu'),
                                       default=True)

    # Social networks
    facebook = models.URLField(verbose_name=ugettext(u"Facebook URL"),
                               max_length=255,
                               blank=True,
                               null=True)

    def __unicode__(self):
        return u"%s" % (self.name)

    class Meta:
        verbose_name = ugettext(u"Kontakt")
        verbose_name_plural = ugettext(u"Kontakty")


class ContactGroup(models.Model):
    """
    Skupina kontaktu
    """
    name = models.CharField(verbose_name=ugettext(u"Jméno"),
                            max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = ugettext(u"Skupina kontaktů")
        verbose_name_plural = ugettext(u"Skupiny kontaktů")
