# -*- coding: utf-8 -*-
from django.conf import settings


RECAPTCHA_PUBLIC_KEY = getattr(settings,
                               "RECAPTCHA_PUBLIC_KEY",
                               "")

RECAPTCHA_SECRET_KEY = getattr(settings,
                               "RECAPTCHA_SECRET_KEY",
                               "")

RECAPTCHA_MAILHIDE_PUBLIC_KEY = getattr(settings,
                                        "RECAPTCHA_MAILHIDE_PUBLIC_KEY",
                                        "")

RECAPTCHA_MAILHIDE_SECRET_KEY = getattr(settings,
                                        "RECAPTCHA_MAILHIDE_SECRET_KEY",
                                        "")

CONTACT_USE_RECAPTCHA_MAILHIDE = getattr(settings,
                                         "CONTACT_USE_RECAPTCHA_MAILHIDE",
                                         False)

CONTACT_SAFEMAIL_REPLACE = getattr(settings,
                                   "CONTACT_SAFEMAIL_REPLACE",
                                   u"(zavináč)")
