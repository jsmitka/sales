# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

from views import list, mail, MailSendedView


urlpatterns = patterns('',
    url(r'^$', list, name="contact-list"),
    url(r'^mail/$', mail, name="contact-mail"),
    url(r'^mail/sended/$', MailSendedView.as_view(), name="contact-mail-sended"),
)
