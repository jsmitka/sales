# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, get_list_or_404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.cache import never_cache
from django.views.generic.base import TemplateView


from forms import MailForm
from models import Contact


def list(request, template="contact/list.html", extra_context=None):

    con = get_list_or_404(Contact)

    return render_to_response(template,
                              {"contacts": con},
                              context_instance=RequestContext(request))


def detail(request, pk, template="contact/detail.html", extra_context=None):
    contact = get_object_or_404(Contact, pk)

    return render_to_response(template,
                              {"contact": contact},
                              context_instance=RequestContext(request))


@never_cache
def mail(request, template="contact/mail.html", extra_context=None):
    if request.method == "POST":
        mail_form = MailForm(remote_ip=request.META['REMOTE_ADDR'], data=request.POST)
        if mail_form.is_valid():
            #TODO: Odeslat mail pres objekt EmailMessage.
            send_mail(mail_form.cleaned_data["subj"],
                      mail_form.cleaned_data["msg"],
                      mail_form.cleaned_data["email_from"],
                      [
                          mail_form.cleaned_data["recipient"].email,
                      ],
                      fail_silently=False,
                      auth_user=settings.EMAIL_HOST_USER,
                      auth_password=settings.EMAIL_HOST_PASSWORD)
            return HttpResponseRedirect(reverse("contact-mail-sended"))

    else:
        mail_form = MailForm(remote_ip=request.META['REMOTE_ADDR'])

    return render_to_response(template,
                              {"form": mail_form},
                              context_instance=RequestContext(request))


class MailSendedView(TemplateView):
    template_name = "contact/mail_sended.html"
