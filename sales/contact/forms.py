# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext

from sales.generic_forms import recaptcha
from models import Contact

class MailForm(recaptcha.RecaptchaForm):
    recipient = forms.ModelChoiceField(required=True, 
                                       label=ugettext(u"Komu"),
                                       queryset=Contact.objects.all())
    email_from = forms.EmailField(required=True, label=ugettext(u"Od"), min_length=5)
    subj = forms.CharField(required=True, label=ugettext(u"Předmět"))
    msg = forms.CharField(required=True, label=ugettext(u"Zpráva"), 
                          widget=forms.Textarea, max_length=255)
    captcha = recaptcha.RecaptchaField()
