# -*- coding: utf-8 -*-
from django import template
from django.template import Context
from django.template import Library

from sales.contact.conf import settings
from sales.contact.models import Contact
from sales.contact.forms import MailForm


register = Library()


@register.simple_tag
def safe_email(email):
    return email.replace("@", settings.CONTACT_SAFEMAIL_REPLACE)


@register.inclusion_tag("contact/snippets/contacts.html", takes_context=True)
def get_contacts(context, count=1):
    contacts = Contact.objects.all()[:count]
    context.update({"contacts": contacts})
    return context


@register.inclusion_tag("contact/snippets/contact.html", takes_context=True)
def get_contact(context, pk=1):
    contact = Contact.objects.get(pk=pk)
    context.update({"contact": contact})
    return context


@register.inclusion_tag("contact/snippets/form.html", takes_context=True)
def get_mail_form(context):
    request = context["request"]
    form = MailForm(remote_ip=request.META['REMOTE_ADDR'])
    context.update({"form": form})
    return context
