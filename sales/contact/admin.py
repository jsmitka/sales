# -*- coding: utf-8 -*-

from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin

from models import Contact, ContactGroup

class ContactAdmin(AdminImageMixin, admin.ModelAdmin):
    pass

admin.site.register(Contact, ContactAdmin)

class ContactGroupAdmin(admin.ModelAdmin):
    pass

admin.site.register(ContactGroup, ContactGroupAdmin)
