# -*- coding: utf-8 -*-
from django import template
from django.template import Context
from django.template import Library

from sales.multimedia.models import ImageFile

register = Library()


@register.inclusion_tag("multimedia/snippets/images_list.html", takes_context=True)
def get_images_for_user(context, user):
    user_images = ImageFile.objects.filter(user=user)
    context.update({"user_images": user_images})
    return context
