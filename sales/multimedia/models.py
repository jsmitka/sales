# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext
from sorl.thumbnail import ImageField
from taggit.managers import TaggableManager


# Create your models here.
class ImageFile(models.Model):
    """
    This class represents image file.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False)

    image = ImageField(verbose_name=ugettext(u"Obrázek"), upload_to="imgs/")
    name = models.CharField(verbose_name=ugettext(u"Název"), blank=True, null=True, max_length=255)
    description = models.TextField(verbose_name=ugettext(u"Popis"), blank=True, null=True)

    tags = TaggableManager(blank=True)

    def __unicode__(self):
        """
        Returns unicode string
        """
        return self.image.name

    class Meta(object):
        """
        This class represents metainformation
        """
        verbose_name = ugettext(u"Obrázek")
        verbose_name_plural = ugettext(u"Obrázky")


class OtherFile(models.Model):
    """
    This class represents other files.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False)

    attachment = models.FileField(verbose_name=ugettext(u"Příloha"), upload_to="others/")
    name = models.CharField(verbose_name=ugettext(u"Název"), blank=True, null=True, max_length=255)
    description = models.TextField(verbose_name=ugettext(u"Popis"), blank=True, null=True)

    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return self.attachment.name

    class Meta(object):
        """
        This class represents metainformation
        """
        verbose_name = ugettext(u"Přiloha")
        verbose_name_plural = ugettext(u"Přílohy")

