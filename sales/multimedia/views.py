# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, get_list_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from models import ImageFile
from forms import ImageFileForm
from conf import settings

from sales.generic_forms.forms import DeleteForm


# Create your views here.
def show(request, template="", extra_context=None):
    pass


#TODO: Omezit pristup k seznamu obrazku.
def multimedia_list(request, template="multimedia/list.html", extra_context=None):
    images_list = get_list_or_404(ImageFile)

    paginator = Paginator(images_list, settings.MULTIMEDIA_ITEMS_PER_PAGE)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        imgs = paginator.page(page)
    except (EmptyPage, InvalidPage):
        imgs = paginator.page(paginator.num_pages)

    return render_to_response(template,
                              {"images": imgs},
                              context_instance=RequestContext(request))


@login_required
def edit(request, id=None, template="multimedia/image_edit.html", extra_context=None):
    if id:
        image = ImageFile.objects.get(pk=id, user=request.user)
    else:
        image = None
    
    
    if request.method == "POST":
        image_form = ImageFileForm(request.POST, request.FILES, instance=image)

        if image_form.is_valid():
            image = image_form.save(commit=False)
            image.user = request.user
            image.save()
            return HttpResponseRedirect(reverse("account-profile"))
    else:
        image_form = ImageFileForm(instance=image)

    return render_to_response(template,
                              {"image_form": image_form},
                              context_instance=RequestContext(request))


@login_required
def delete(request, id, template="delete.html", extra_context=None):
    image = get_object_or_404(ImageFile, pk=id)

    if request.method == "POST":
        delete_form = DeleteForm(request.POST)
        if delete_form.is_valid():
            image.delete()
        return HttpResponseRedirect(reverse("account-profile"))
    else:
        delete_form = DeleteForm()

    return render_to_response(template,
                              {"form": delete_form, "object": image},
                              context_instance=RequestContext(request))

