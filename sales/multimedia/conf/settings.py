# -*- coding: utf-8 -*-
from django.conf import settings

MULTIMEDIA_ITEMS_PER_PAGE = getattr(settings,
                                    "MULTIMEDIA_ITEMS_PER_PAGE",
                                    20)
