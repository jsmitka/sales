# -*- coding: utf-8 -*-

from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin

from models import ImageFile, OtherFile

class ImageFileAdmin(AdminImageMixin, admin.ModelAdmin):
    """
    This class represents admin for person
    """
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

    list_display = ("image", "name", "user",)

admin.site.register(ImageFile, ImageFileAdmin)


class OtherFileAdmin(admin.ModelAdmin):
    """
    This class represents admin for person
    """
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

    list_display = ("attachment", "name", "user",)

admin.site.register(OtherFile, OtherFileAdmin)
