# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext
from django.contrib.auth.models import User

from models import ImageFile


class ImageFileForm(forms.ModelForm):
    class Meta(object):
        model = ImageFile
        fields = ("name", "image", "description")
