# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from views import show, edit, delete, multimedia_list

urlpatterns = patterns('',
    # Vypis clanku
    url(r'^images/$', multimedia_list,  {"template": "multimedia/list_simple.html"}, name="multimedia-image-list"),
    url(r'^image/add/$', edit, name="multimedia-image-add"),
    url(r'^image/(?P<id>\d+)/delete/$', delete, name="multimedia-image-delete"),
)
