from django.template import Library, Node, TemplateSyntaxError
from django.db.models import get_model

register = Library()

class LatestContentNode(Node):
    def __init__(self, model, num, varname):
        self.num, self.varname = num, varname
        self.model = get_model(*model.split('.'))

    def render(self, context):
        try:
            context[self.varname] = self.model._default_manager.filter(enable_on_home=True).order_by("-pk")
        #FIX: Zachyceni vyjimky FieldError, ale nemuzu najit, kde je definovana, tak se zde chytaji vsechny
        except:
            context[self.varname] = self.model._default_manager.all().order_by("-pk")[:self.num]
        return ''

@register.tag 
def get_latest(parser, token):
    bits = token.contents.split()
    if len(bits) != 5:
        raise TemplateSyntaxError, "get_latest tag takes exactly four arguments"
    if bits[3] != 'as':
        raise TemplateSyntaxError, "third argument to get_latest tag must be 'as'"
    return LatestContentNode(bits[1], bits[2], bits[4])


class SelectContentNode(Node):
    """
    This class represents filter content
    """

    def __init__(self, model, num, varname, filter, filter_value):
        """
        Constructor.
        
        model -- model
        num -- pocet vracenych objektu
        varname -- jmeno promene vracene do contextu
        filter -- pres co vybrat objekt (tedy vyfiltrovat)
        filter_value -- hodnota
        """
        #assert model 
        #assert num 
        #assert varname 
        #assert filter 
        #assert filter_value 
        self._model = model
        self._num = num
        self._varname = varname
        self._query = {"%s"%filter: filter_value}
        
    def render(self, context):
        context[self._varname] = self._model._default_manager.filter(**self._query)[:self._num]
